//
//  AGTAirportCalloutView.h
//  AirportsMapKit
//
//  Created by Fernando Rodríguez Romero on 23/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTAirportCalloutView : UIView
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *iata;



@end
