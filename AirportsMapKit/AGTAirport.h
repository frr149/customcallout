//
//  AGTAirport.h
//  AirportsMapKit
//
//  Created by Fernando Rodríguez Romero on 23/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AGTAirport : NSObject <MKAnnotation>

@property (nonatomic, copy,readonly) NSString *title;
@property (nonatomic, copy,readonly) NSString *subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id) initWithName:(NSString*)name
          iataCode:(NSString*) iata
          latitude:(double) latitude
         longitude:(double) latitude;

@end
