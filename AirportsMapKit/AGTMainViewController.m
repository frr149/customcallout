//
//  AGTMainViewController.m
//  AirportsMapKit
//
//  Created by Fernando Rodríguez Romero on 23/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTMainViewController.h"
#import "AGTAirport.h"
#import "AGTAirportCalloutView.h"
#import "AGTAirportPinView.h"

@interface AGTMainViewController ()<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocation *initialLocation;
@end

@implementation AGTMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.initialLocation = [[CLLocation alloc] initWithLatitude:40.49027
                                                      longitude:-3.564479];
    
    [self centerOnLocation:self.initialLocation];
    
    [self.mapView addAnnotation:[[AGTAirport alloc] initWithName:@"Adolfo Suárez"
                                                        iataCode:@"MAD"
                                                        latitude:40.49027
                                                       longitude:-3.564479]];
    
    self.mapView.delegate = self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) centerOnLocation:(CLLocation*) loc{
    
    CLLocationDistance radius = 18000;
    
    MKCoordinateRegion r = MKCoordinateRegionMakeWithDistance(self.initialLocation.coordinate, radius, radius);
    
    [self.mapView setRegion:r];
    
}


#pragma mark - MKMapViewDelegate
-(MKAnnotationView*)mapView:(MKMapView *)mapView
          viewForAnnotation:(id<MKAnnotation>)annotation{
    
    // Muestra la chincheta. Funcionamiento similar a las tablas y las celdas
    if ([annotation isKindOfClass:[AGTAirport class]]) {
        
        
        AGTAirportPinView *v;
        static NSString *AnnotationId = @"AirportAnnotation";
        
        v = (AGTAirportPinView*)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationId];
        if (!v) {
            // Hay que crearla de cero
            v = [[AGTAirportPinView alloc] initWithAnnotation:annotation
                                             reuseIdentifier:AnnotationId];
            
        }
        
        v.canShowCallout = NO;
        v.pinColor = MKPinAnnotationColorGreen;
        return v;
        
    }else{
        return nil;
    }
}

-(void) mapView:(MKMapView *)mapView
didSelectAnnotationView:(MKAnnotationView *)view{
    
    AGTAirport *ap = view.annotation;
    NSLog(@"Aeropuerto: %@", ap);
    
    
    // Custom Callout
    NSArray *nibContents = [[NSBundle mainBundle]loadNibNamed:@"AGTAirportCalloutView"
                                                        owner:self options:nil];
    
    
    AGTAirportCalloutView *c = [nibContents firstObject];
    
    c.name.text = ap.title;
    c.iata.text = ap.subtitle;
    c.bounds = CGRectMake(0, 0, 300, 64);
    
    c.center = CGPointMake(view.bounds.size.width*0.5f, view.bounds.size.height*0.5f - 60);
    
    [view addSubview:c];
    
    
    
    
}


@end
