//
//  AGTAirportPinView.h
//  AirportsMapKit
//
//  Created by Fernando Rodríguez Romero on 23/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "AGTAirportCalloutView.h"

@interface AGTAirportPinView : MKPinAnnotationView

@property (strong, nonatomic) AGTAirportCalloutView *callout;

@end
