//
//  AGTAirport.m
//  AirportsMapKit
//
//  Created by Fernando Rodríguez Romero on 23/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTAirport.h"

@interface AGTAirport ()
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *iataCode;
@property (nonatomic,strong) CLLocation *location;
@end
@implementation AGTAirport

-(CLLocationCoordinate2D)coordinate{
    return self.location.coordinate;
}

-(NSString*) title{
    return self.name;
}

-(NSString *) subtitle{
    return [NSString stringWithFormat:@"IATA Code: %@", self.iataCode];
}

-(id) initWithName:(NSString*)name
          iataCode:(NSString*) iata
          latitude:(double) latitude
         longitude:(double) longitude{
    
    if (self = [super init]) {
        _name = name;
        _iataCode = iata;
        _location = [[CLLocation alloc] initWithLatitude:latitude
                                               longitude:longitude];
    }
    return self;
}



-(NSString*) description{
    return [NSString stringWithFormat:@"<%@: %@ (%@)>",
            [self class],
            [self name],
            [self iataCode] ];
    
}
@end
